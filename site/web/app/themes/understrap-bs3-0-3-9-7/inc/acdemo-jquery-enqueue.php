<?php
/*
 * Function: acdemo_jquery_enque
 * Arguments: none
 * Description: Load jQuery from Google CDN if available otherwise include the local copy
 * Return value: none
 * Called from: Understrap BS3 enqueue script, which enqueues all understrap scripts via wp_enqueue_scripts
 */
function acdemo_jquery_enqueue() {
    // Bootstrap 3's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3
    //$url = 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'; // URL to check
    $url = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'; // URL to check
    $test_url = @fopen($url,'r'); // try to open a connection

    if($test_url !== false) { // test if the URL exists
        wp_deregister_script( 'jquery' ); // deregister default jQuery installed with WordPress
        //wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'); // register the external file
        wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'); // register the external file
        wp_enqueue_script('jquery'); // enqueue the CDN hosted version
    } else {
        wp_enqueue_script('jquery'); // enqueue the local version
    }
} // acdemo_jquery_enqueue()
