<?php
/**
 * Template Name: Front Page
 *
 * Template for displaying the AlgaeCal product page demo
 */

//get_header(); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body <?php body_class('brand_green product-template-default single single-product postid-44079 custom-background woocommerce woocommerce-page desktop page-algaecal-plus wpb-js-composer js-comp-ver-5.2 vc_responsive'); ?>>
  <div class="site-wrapper">
		<div id="container">
			<div id="content" role="main">
				<div class="container">
					<div class="row">
						<div id="product-44079" class="post-44079 product type-product status-publish has-post-thumbnail product_cat-parent-product product_tag-algaecal product_tag-algaecal-plus brand_green first instock shipping-taxable product-type-grouped">
							<div class="woocommerce-tabs wc-tabs-wrapper">
								<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--Description panel entry-content wc-tab" id="tab-Description" role="tabpanel">
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-12 visible-sm-block">
                                                <?php while ( have_posts() ) : the_post(); ?>
                                                <?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
												<h2 class="subheading">
													A proven, doctor-approved 100% natural solution for women with osteoporosis
												</h2>
											</div>
                                            <?php the_content(); ?>
											<div class="col-xs-12 col-sm-6 col-lg-5 product-image-column">
												<div class="images clearfix">
													<div class="col-sm-9 col-md-12 col-sm-push-3 col-md-push-0">
														<div class="product-gallery-images">
															<div class='woocommerce-main-image product-image zoom-image' data-initial="true">
																<img width="700" height="700" src="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal_F-700x700.png" class="attachment-700x700 size-700x700" alt="AlgaeCal Plus Single Bottle" data-full="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal_F-1024x1024.png" />
															</div>
															<div class='woocommerce-main-image product-image zoom-image'>
																<img width="700" height="700" src="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal_R-700x700.png" class="attachment-700x700 size-700x700" alt="AlgaeCal Plus Supplement Facts" data-full="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal_R-1024x1024.png" />
															</div>
															<div class='woocommerce-main-image product-image'>
																<a data-target='#7yearmodal' data-toggle='modal'>
																	<img width="700" height="700" src="https://cdn.algaecal.com/wp-content/uploads/guarantee-square-pattern-700x700.png" class="attachment-700x700 size-700x700" alt="7 Year Money Back Guarantee (click for details)" data-full="https://cdn.algaecal.com/wp-content/uploads/guarantee-square-pattern.png" />
																</a>
															</div>
															<div class='woocommerce-main-image product-image zoom-image'>
																<img width="700" height="700" src="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal-Plus-Capsule-700x700.jpg" class="attachment-700x700 size-700x700" alt="AlgaeCal Plus Capsule" data-full="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal-Plus-Capsule-1024x1024.jpg" />
															</div>
															<div class='woocommerce-main-image product-image zoom-image'>
																<img width="700" height="700" src="https://cdn.algaecal.com/wp-content/uploads/Lifestyle-AP-For-Website-05700-700x700.jpg" class="attachment-700x700 size-700x700" alt="AlgaeCal Plus" data-full="https://cdn.algaecal.com/wp-content/uploads/Lifestyle-AP-For-Website-05700-1024x1024.jpg" />
															</div>
															<div class='woocommerce-main-image product-image zoom-image'>
																<img width="700" height="700" src="https://cdn.algaecal.com/wp-content/uploads/algae-behind-the-results-700x700.png" class="attachment-700x700 size-700x700" alt="The Algae Behind the Results" data-full="https://cdn.algaecal.com/wp-content/uploads/algae-behind-the-results-1024x1024.png" />
															</div>
															<div class="product-video product-image">
																<div class="embed-responsive embed-responsive-16by9">
																	<script src="//fast.wistia.com/embed/medias/w4ithbv9tz.jsonp" async></script>
																	<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
																	<div class="wistia_embed wistia_async_w4ithbv9tz" style="height:395px;width:700px">
																		&nbsp;
																	</div>
																</div>
															</div>
														</div>
<!-- .product-images -->
													</div>
<!-- .col-sm-9 -->
<!-- <div class="col-xs-12"> -->
													<div class="col-sm-3 col-md-12 col-sm-pull-9 col-md-pull-0">
														<div class="product-gallery-thumbnails hidden-xs">
															<div class='product-thumbnail product-image-thumbnail' data-index='0'>
																<img width="300" height="300" src="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal_F-300x300.png" class="attachment-300x300 size-300x300" alt="" />
															</div>
															<div class='product-thumbnail product-image-thumbnail' data-index='1'>
																<img width="300" height="300" src="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal_R-300x300.png" class="attachment-300x300 size-300x300" alt="" />
															</div>
															<div class='product-thumbnail product-image-thumbnail' data-index='2'>
																<img width="300" height="300" src="https://cdn.algaecal.com/wp-content/uploads/guarantee-square-pattern-300x300.png" class="attachment-300x300 size-300x300" alt="" />
															</div>
															<div class='product-thumbnail product-image-thumbnail' data-index='3'>
																<img width="300" height="300" src="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal-Plus-Capsule-300x300.jpg" class="attachment-300x300 size-300x300" alt="" />
															</div>
															<div class='product-thumbnail product-image-thumbnail' data-index='4'>
																<img width="300" height="300" src="https://cdn.algaecal.com/wp-content/uploads/Lifestyle-AP-For-Website-05700-300x300.jpg" class="attachment-300x300 size-300x300" alt="" />
															</div>
															<div class='product-thumbnail product-image-thumbnail' data-index='5'>
																<img width="300" height="300" src="https://cdn.algaecal.com/wp-content/uploads/algae-behind-the-results-300x300.png" class="attachment-300x300 size-300x300" alt="" />
															</div>
															<div class='product-thumbnail product-wistia-thumbnail' data-index='6'>
																<div class="">
																	<img alt="video" src="https://embed-ssl.wistia.com/deliveries/39bfe29761db5df7b57479c40ae7246b2aea7ed3.jpg?image_crop_resized=300x168" data-wistia-thumb="http://fast.wistia.net/oembed?url=http://home.wistia.com/medias/w4ithbv9tz?embedType=async&videoWidth=300" />
																</div>
															</div>
														</div>
<!-- .product-gallery-thumbnails -->
													</div>
<!-- .col-sm-3 -->
<!-- </div> -->
<!--.col-xs-12 -->
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-lg-7 product-summary-column">
												<div class="summary entry-summary">
                                                    <?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
                                                    <?php endwhile; // end of the loop. ?>
													<h2 class="subheading">
														A proven, doctor-approved 100% natural solution for women with osteoporosis
													</h2>
                          <div class="yotpo bottomLine yotpo-medium" data-product-id="44079" data-url="https://www.algaecal.com/product/algaecal-plus/" data-lang="en" data-yotpo-element-id="2">
  													<div class="yotpo-display-wrapper">
    													<div class="standalone-bottomline">
    														<div class="yotpo-bottomline pull-left star-clickable">
    															<span class="yotpo-stars"> <span class="yotpo-icon yotpo-icon-star pull-left"></span><span class="yotpo-icon yotpo-icon-star pull-left"></span><span class="yotpo-icon yotpo-icon-star pull-left"></span><span class="yotpo-icon yotpo-icon-star pull-left"></span><span class="yotpo-icon yotpo-icon-star pull-left"></span> </span> <a class="text-m">206 Reviews</a>
    															<div class="yotpo-clr">
    															</div>
    														</div>
    														<div class="yotpo-clr">
    														</div>
    													</div>
    													<div class="yotpo-clr">
    													</div>
                            </div>
  												</div>
													<section class="important-points">
														<dl>
															<dt>
																Plant-Based
															</dt>
															<dd>
																Super absorbable calcium, magnesium and trace minerals from algae!
															</dd>
															<dt>
																Guaranteed
															</dt>
															<dd>
																To increase bone density in 6 months when taken with Strontium Boost!
															</dd>
															<dt>
																Clinical doses
															</dt>
															<dd>
																Of vitamins D3, K2, boron and magnesium!
															</dd>
														</dl>
													</section>
												</div>
											</div>
											<div class="col-xs-12 col-md-6 col-lg-7 col-md-push-6 col-lg-push-0">
												<div class="directions hidden-xs">
													<table>
														<tbody>
															<tr>
																<th>
																</th>
																<th>
																	AlgaeCal Plus
																</th>
															</tr>
															<tr>
																<td>
																	<strong>
																		Description:
																	</strong>
																</td>
																<td>
																	90 easy to swallow veggie capsules
																</td>
															</tr>
															<tr>
																<td>
																	<strong>
																		Suggested Use:
																	</strong>
																</td>
																<td>
																	4 caps daily (2 caps twice a day with meals)
																</td>
															</tr>
															<tr>
																<td>
																	<strong>
																		Availability:
																	</strong>
																</td>
																<td>
																	<span class="ac_blue">
																		<strong>
																			IN STOCK
																		</strong>
																		leaves warehouse in 1 business day
																	</span>
																</td>
															</tr>
															<tr>
																<td>
																	<strong>
																		Shipping Fee:
																	</strong>
																</td>
																<td>
																	<span class="ac_green">
																		<u>
																			Free Shipping Everywhere
																		</u>
																	</span>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
                        <div class="row share-with-others">
                          <div class="col-xs-12 col-sm-6">
                              <a class="addthis_button_email at300b" target="_blank" title="Email" href="#"><span class="at-icon-wrapper"></span>Tell a Friend</a> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-520579b523a0a5bb">
                              </script>
                          </div>
                          <div class="col-xs-12 col-sm-6">
                              <a data-toggle="modal" class="dr-note icon-file-pdf ga-take-this-to-your-doctor" data-target="#pdf_modal" data-source="https://cdn.algaecal.com/wp-content/uploads/AlgaeCal-Take-It-To-Your-Dr-Form.pdf" href="#">Take This to Your Doctor</a>
                          </div>
                        </div>
											</div>
<!-- .summary -->
										</div>
										<div class="order_information col-xs-12 visible-xs">
											<p>
												<strong>
													Important:
												</strong>
											</p>
											<ul>
												<li>
													ALL products Include
													<span class="green-caps">
														FREE SHIPPING
													</span>
													Everywhere
												</li>
												<li>
													Orders Leave Warehouse in 1 Business Day
												</li>
												<li>
													Your &#8216;Stronger Bones for 7 Years&#8217; Guarantee&#8230;
												</li>
											</ul>
											<br />
											<em>
												Take a DXA scan to determine your bone density now. Then follow directions for use of AlgaeCal Plus and Strontium Boost for at least 6 months before taking another scan. We guarantee you increased bone density in EVERY follow-up DXA scan for the next 7 years with continued use of these 2 products! If not, we will refund every penny you paid for our products between your scans. You MUST WIN your battle against bone loss or be fully refunded!
											</em>
										<p>
											<strong>
												Guarantee Exceptions
											</strong>
										</p>
										<ol>
											<li>
												<em>
													Drug Use: many prescription drugs interfere with bone density. Call us toll free to learn if your prescription excludes you from our guarantee.
													<a href="tel:+18008200184">
														1-800-820-0184
                          </a>
													</em>
											</li>
											<li>
												<em>
													Early Stage Menopause: for the first five years of menopause, hormone shifts can offset the bone building benefits from our products.
												</em>
											</li>
										</ol>
										<p>
											<em>
												If you are in either of these exceptions, the nutrients in our products are EVEN MORE IMPORTANT to take to minimize your bone loss! And often you will STILL increase your bone density in spite of medication use or early menopause.
											</em>
											<br />
										</div>
									</div>
									<div class="row">
										<div aria-hidden="true" class="clearfix">
										</div>
										<div class="col-sm-8 col-sm-offset-2 col-md-offset-0 col-md-4 purchase-option-column" id="bundle-0">
											<div class="product-box-wrapper">
												<div class="product-box">
													<div class="product-box-header">
														<h3 class="bundle-product-name">
															1 Bottle
														</h3>
<!-- #bundle-product-name -->
														<div class="price-per-day">
															Only
															<span class="woocommerce-Price-amount amount">
																<span class="woocommerce-Price-currencySymbol">
																	&#36;
																</span>
																2.22
															</span>
															per Day!
														</div>
													</div>
<!-- .product-box-header -->
													<div class="product-box-content">
														<div class="product-image">
															<img width="860" height="515" src="//www.algaecal.com/wp-content/uploads/AP-1.png" class="attachment-large size-large wp-post-image" alt="" />
														</div>
<!-- .product-image -->
													</div>
<!-- .product-box-content -->
													<div class="product-box-footer">
														<div class="price">
															<span class="woocommerce-Price-amount amount">
																<span class="woocommerce-Price-currencySymbol">
																	&#36;
																</span>
																50
															</span>
															per Bottle
														</div>
<!-- .price -->
														<div class="pricing-info">
															<span class="todays-price">
																<strong>
																	Today's Price:
																</strong>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">
																		&#36;
																	</span>
																	50
																</span>
																<span class="currency">
																	USD
																</span>
															</span>
															<br />
														</div>
<!-- .pricing-info -->
														<a rel="nofollow" data-quantity="1" data-product_id="15497" data-product_sku="AlgaeCal Plus Silver" class="btn button btn-with-seal product_type_simple add-to-cart-btn-green add_to_cart_aw ">
															<img class="seal" src="https://cdn.algaecal.com/wp-content/uploads/7_yr_bone_guarantee_seal.png" alt="7 year guarantee">
															<span class="arrow">
																Add to cart
															</span>
														</a>
													</div>
<!-- .product-box-footer -->
												</div>
<!-- .product-box -->
											</div>
<!-- .product-box-wrapper -->
										</div>
<!-- .purchase-option-column -->
										<div class="col-sm-8 col-sm-offset-2 col-md-offset-0 col-md-4 purchase-option-column" id="bundle-1">
											<div class="product-box-wrapper">
												<div class="product-box">
													<div class="product-box-header">
														<h3 class="bundle-product-name">
															3 Month Supply
														</h3>
<!-- #bundle-product-name -->
														<h4 class="bundle-product-description">
															4 Bottles
														</h4>
														<div class="price-per-day">
															Only
															<span class="woocommerce-Price-amount amount">
																<span class="woocommerce-Price-currencySymbol">
																	&#36;
																</span>
																1.89
															</span>
															per Day!
														</div>
													</div>
<!-- .product-box-header -->
													<div class="product-box-content">
														<div class="product-image">
															<img width="860" height="515" src="//www.algaecal.com/wp-content/uploads/AP-3.png" class="attachment-large size-large wp-post-image" alt="" />
															<div class="percent-off">
																<div class="label">
																	<span class="amount">
																		15%
																	</span>
																	Off
																</div>
<!-- .label -->
															</div>
<!-- .percent-off -->
														</div>
<!-- .product-image -->
													</div>
<!-- .product-box-content -->
													<div class="product-box-footer">
														<div class="price">
															<span class="woocommerce-Price-amount amount">
																<span class="woocommerce-Price-currencySymbol">
																	&#36;
																</span>
																43
															</span>
															per Bottle
														</div>
<!-- .price -->
														<div class="pricing-info">
															<span class="retail-price">
																<strong>
																	Retail Price:
																</strong>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">
																		&#36;
																	</span>
																	200
																</span>
															</span>
															<br />
															<span class="todays-price">
																<strong>
																	Today's Price:
																</strong>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">
																		&#36;
																	</span>
																	170
																</span>
																<span class="currency">
																	USD
																</span>
															</span>
															<br />
														</div>
<!-- .pricing-info -->
														<a rel="nofollow" data-quantity="1" data-product_id="15498" data-product_sku="AlgaeCal Plus 3 Month Supply" class="btn button btn-with-seal product_type_simple add-to-cart-btn-green add_to_cart_aw ">
															<img class="seal" src="https://cdn.algaecal.com/wp-content/uploads/7_yr_bone_guarantee_seal.png" alt="7 year guarantee">
															<span class="arrow">
																Add to cart
															</span>
														</a>
													</div>
<!-- .product-box-footer -->
												</div>
<!-- .product-box -->
											</div>
<!-- .product-box-wrapper -->
										</div>
<!-- .purchase-option-column -->
										<div class="col-sm-8 col-sm-offset-2 col-md-offset-0 col-md-4 purchase-option-column" id="bundle-2">
											<div class="product-box-wrapper most-popular">
												<div class="most-popular-label text-center">
													Most Popular
												</div>
<!-- .most-popular-label -->
												<div class="product-box">
													<div class="product-box-header">
														<h3 class="bundle-product-name">
															6 Month Supply
														</h3>
<!-- #bundle-product-name -->
														<h4 class="bundle-product-description">
															8 Bottles
														</h4>
														<div class="price-per-day">
															Only
															<span class="woocommerce-Price-amount amount">
																<span class="woocommerce-Price-currencySymbol">
																	&#36;
																</span>
																1.54
															</span>
															per Day!
														</div>
													</div>
<!-- .product-box-header -->
													<div class="product-box-content">
														<div class="product-image">
															<img width="860" height="515" src="//www.algaecal.com/wp-content/uploads/AP-6.png" class="attachment-large size-large wp-post-image" alt="" />
															<div class="percent-off">
																<div class="label">
																	<span class="amount">
																		30%
																	</span>
																	Off
																</div>
<!-- .label -->
															</div>
<!-- .percent-off -->
														</div>
<!-- .product-image -->
													</div>
<!-- .product-box-content -->
													<div class="product-box-footer">
														<div class="price">
															<span class="woocommerce-Price-amount amount">
																<span class="woocommerce-Price-currencySymbol">
																	&#36;
																</span>
																35
															</span>
															per Bottle
														</div>
<!-- .price -->
														<div class="pricing-info">
															<span class="retail-price">
																<strong>
																	Retail Price:
																</strong>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">
																		&#36;
																	</span>
																	400
																</span>
															</span>
															<br />
															<span class="todays-price">
																<strong>
																	Today's Price:
																</strong>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">
																		&#36;
																	</span>
																	277
																</span>
																<span class="currency">
																	USD
																</span>
															</span>
															<br />
														</div>
<!-- .pricing-info -->
														<a rel="nofollow" data-quantity="1" data-product_id="15499" data-product_sku="AlgaeCal Plus 6 Month Supply" class="btn button btn-with-seal product_type_simple add-to-cart-btn-green add_to_cart_aw ">
															<img class="seal" src="https://cdn.algaecal.com/wp-content/uploads/7_yr_bone_guarantee_seal.png" alt="7 year guarantee">
															<span class="arrow">
																Add to cart
															</span>
														</a>
													</div>
<!-- .product-box-footer -->
												</div>
<!-- .product-box -->
											</div>
<!-- .product-box-wrapper -->
										</div>
<!-- .purchase-option-column -->
										<div aria-hidden="true" class="clearfix">
										</div>
									</div>
<!-- .row -->
								</div>
							</div>
						</div>
<!-- #product-44079 -->
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- site-wrapper -->
<?php wp_footer(); ?>
</body>
</html>
